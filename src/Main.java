import javax.swing.*;

/**
 * Created by lasse on 2/24/14.
 */
public class Main {
    public static void main(String[] args) {
        //Create JFrame
        JFrame frame = new JFrame("Næstved Vejtransport - Administrationssytem");
        frame.setContentPane(new WindowFrame().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }
}
